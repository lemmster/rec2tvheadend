/*
 * Copyright (c) 2013, Markus Alexander Kuppe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.kuppe.tvheadend.channel.internal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import org.kuppe.tvheadend.channel.IChannelMapper;

public class ChannelMapper implements IChannelMapper {

	private final Map<String, Integer> channel2Id = new HashMap<String, Integer>();

	public ChannelMapper() {
		final Properties props = new Properties();
		try {
			String userHome = System.getProperty("user.home");
			props.load(new FileInputStream(userHome + File.separator + ".ical4tvheadend" + File.separator
					+ "channel2id.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Set<Entry<Object, Object>> entrySet = props.entrySet();
		for (Entry<Object, Object> entry : entrySet) {
			String key = entry.getKey().toString();
			int value = Integer.parseInt(entry.getValue().toString());
			channel2Id.put(key, value);
		}
	}

	public int getChannelId(final String aChannelName) {
		if (channel2Id.containsKey(aChannelName)) {
			return channel2Id.get(aChannelName);
		} else {
			// Unknown channel
			System.err.println("No channel mapping for " + aChannelName);
			return -1;
		}
	}
	public String getChannelName(final int aChannelId) {
		for (Entry<String, Integer> entry : channel2Id.entrySet()) {
			if (entry.getValue() == aChannelId) {
				return entry.getKey();
			}
		}
		// Unknown channel
		System.err.println("No channel mapping for id " + aChannelId);
		return "Unkown Channel";
	}
}
