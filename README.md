# To build, manually run:

# Download XRay from http://jpm4j.org/#!/p/sha/9E6EB358FF2160761679A553D55DC6235C848FD0//0.0.0 and install into local maven repo
wget http://repo.jpm4j.org/rest/bundle/51C83986E4B06EF1574B84F7/9e6eb358ff2160761679a553d55dc6235c848fd0 -O aQute.xray.plugin.jar
mvn install:install-file -DgroupId=osgi -DartifactId=aQute.xray.plugin -Dversion=1.3.0 -Dpackaging=jar -Dfile=aQute.xray.plugin.jar

# Kick off regular build
mvn clean verify