/*
 * Copyright (c) 2013, Markus Alexander Kuppe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.kuppe.tvheadend.service.callable;

import java.io.NotSerializableException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.ecf.remoteservice.IRemoteCall;
import org.eclipse.ecf.remoteservice.client.IRemoteCallParameter;
import org.eclipse.ecf.remoteservice.client.IRemoteCallParameterSerializer;
import org.eclipse.ecf.remoteservice.client.IRemoteCallable;
import org.eclipse.ecf.remoteservice.client.IRemoteResponseDeserializer;
import org.eclipse.ecf.remoteservice.client.IRemoteServiceClientContainerAdapter;
import org.kuppe.tvheadend.channel.IChannelMapper;

public class TVHeadEndCallables implements IRemoteCallParameterSerializer, IRemoteResponseDeserializer {

	private static List<IRemoteCallable> callables; 
	
	public synchronized IRemoteCallable[] getCallables(IRemoteServiceClientContainerAdapter adapter, IChannelMapper aMapper) {
		if (callables == null) {
			callables = new ArrayList<IRemoteCallable>();
			callables.add(new AddRecordingCallable(aMapper));
			callables.add(new GetAllRecordingsCallable());
			callables.add(new DeleteRecordingCallable());
		}
		return callables.toArray(new IRemoteCallable[callables.size()]);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ecf.remoteservice.client.IRemoteCallParameterSerializer#serializeParameter(java.lang.String, org.eclipse.ecf.remoteservice.IRemoteCall, org.eclipse.ecf.remoteservice.client.IRemoteCallable, org.eclipse.ecf.remoteservice.client.IRemoteCallParameter, java.lang.Object)
	 */
	public IRemoteCallParameter serializeParameter(String endpoint, IRemoteCall call, IRemoteCallable callable,
			IRemoteCallParameter paramDefault, Object paramToSerialize) throws NotSerializableException {
		// dispatch to individual callables (all tvheadend callables are IRCPS)
		if (callable instanceof IRemoteCallParameterSerializer) {
			IRemoteCallParameterSerializer ircps = (IRemoteCallParameterSerializer) callable;
			return ircps.serializeParameter(endpoint, call, callable, paramDefault, paramToSerialize);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ecf.remoteservice.client.IRemoteCallParameterSerializer#serializeParameter(java.lang.String, org.eclipse.ecf.remoteservice.IRemoteCall, org.eclipse.ecf.remoteservice.client.IRemoteCallable, org.eclipse.ecf.remoteservice.client.IRemoteCallParameter[], java.lang.Object[])
	 */
	public IRemoteCallParameter[] serializeParameter(String endpoint, IRemoteCall call, IRemoteCallable callable,
			IRemoteCallParameter[] paramDefault, Object[] paramToSerialize) throws NotSerializableException {
		if (callable instanceof IRemoteCallParameterSerializer) {
			IRemoteCallParameterSerializer ircps = (IRemoteCallParameterSerializer) callable;
			return ircps.serializeParameter(endpoint, call, callable, paramDefault, paramToSerialize);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ecf.remoteservice.client.IRemoteResponseDeserializer#deserializeResponse(java.lang.String, org.eclipse.ecf.remoteservice.IRemoteCall, org.eclipse.ecf.remoteservice.client.IRemoteCallable, java.util.Map, byte[])
	 */
	public Object deserializeResponse(String endpoint, IRemoteCall call, IRemoteCallable callable,
			@SuppressWarnings("rawtypes") Map responseHeaders, byte[] responseBody) throws NotSerializableException {
		if (callable instanceof IRCWithResponseHandler) {
			IRCWithResponseHandler mrc = (IRCWithResponseHandler) callable;
			return mrc.handleResponse(responseHeaders, responseBody);
		}
		return null;
	}
}
