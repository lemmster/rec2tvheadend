/*
 * Copyright (c) 2013, Markus Alexander Kuppe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.kuppe.tvheadend.service.callable;

import java.io.NotSerializableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.ecf.remoteservice.IRemoteCall;
import org.eclipse.ecf.remoteservice.client.IRemoteCallParameter;
import org.eclipse.ecf.remoteservice.client.IRemoteCallParameterSerializer;
import org.eclipse.ecf.remoteservice.client.IRemoteCallable;
import org.eclipse.ecf.remoteservice.client.RemoteCallParameter;
import org.eclipse.ecf.remoteservice.client.RemoteCallParameterFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.kuppe.tvheadend.service.model.TvHeadEndRecording;

public class DeleteRecordingCallable extends RecordingCallable implements IRemoteCallParameterSerializer {

	private static final String ENTRY_ID = "entryId";
	private static final String METHOD = "deleteRecording";
	private static final String TARGET_RESOURCE_PATH = "/dvr";

	public DeleteRecordingCallable() {
		super(METHOD, TARGET_RESOURCE_PATH, getDefaultParametersInit());
	}

	private static IRemoteCallParameter[] getDefaultParametersInit() {
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("op", "cancelEntry");
		return RemoteCallParameterFactory.createParameters(params);
	}

	/* (non-Javadoc)
	 * @see org.kuppe.tvheadend.service.callable.IRCWithResponseHandler#handleResponse(java.util.Map, byte[])
	 */
	@SuppressWarnings("rawtypes")
	public Object handleResponse(Map responseHeaders, byte[] responseBody) throws NotSerializableException {
		try {
			final JSONObject jsonObject = new JSONObject(new String(responseBody));
			return jsonObject.getInt("success") == 1;
		} catch (JSONException e) {
			throw new NotSerializableException("JSON array parse exception: "+e.getMessage());
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ecf.remoteservice.client.IRemoteCallParameterSerializer#serializeParameter(java.lang.String, org.eclipse.ecf.remoteservice.IRemoteCall, org.eclipse.ecf.remoteservice.client.IRemoteCallable, org.eclipse.ecf.remoteservice.client.IRemoteCallParameter[], java.lang.Object[])
	 */
	public IRemoteCallParameter[] serializeParameter(String endpoint, IRemoteCall call, IRemoteCallable callable,
			IRemoteCallParameter[] currentParam, Object[] paramToSerialize) throws NotSerializableException {
		final List<IRemoteCallParameter> res = new ArrayList<IRemoteCallParameter>();
		res.addAll(Arrays.asList(currentParam));
		for (Object object : paramToSerialize) {
			if (object instanceof TvHeadEndRecording) {
				final TvHeadEndRecording r = (TvHeadEndRecording) object;
				res.add(new RemoteCallParameter(ENTRY_ID, r.getRecordingId()));
			}
		}
		return res.toArray(new IRemoteCallParameter[res.size()]);
	}
}
