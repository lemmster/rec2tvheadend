/*
 * Copyright (c) 2013, Markus Alexander Kuppe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.kuppe.tvheadend.service.callable;

import java.io.NotSerializableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.ecf.remoteservice.IRemoteCall;
import org.eclipse.ecf.remoteservice.client.IRemoteCallParameter;
import org.eclipse.ecf.remoteservice.client.IRemoteCallParameterSerializer;
import org.eclipse.ecf.remoteservice.client.IRemoteCallable;
import org.eclipse.ecf.remoteservice.client.RemoteCallParameter;
import org.eclipse.ecf.remoteservice.client.RemoteCallParameterFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.kuppe.tv.recording.Recording;
import org.kuppe.tvheadend.channel.IChannelMapper;

public class AddRecordingCallable extends RecordingCallable implements IRemoteCallParameterSerializer {

	// TODO frontend sends time in local time zone, server returns UTC
	// (if we start going into a scheduling/deletion cycle again, this might
	// be caused by a change in DST. The root cause is then, that we and the
	// backend are working with different time zones)
	// Ideally, we would send a timestamp (seconds since epoch) instead of
	// strings (here you see that this isn't real API).
	/**
	 * daylightSavingTimeOffset between frontend and (tvheadend) backend
	 */
	private static final Integer DST_OFFSET = Integer.getInteger("TVheadEndService.DST_OFFSET", 0);

	private static final String METHOD = "addRecording";
	private static final String TARGET_RESOURCE_PATH = "/dvr/addentry";

	private final Calendar cal = new GregorianCalendar();
	private final IChannelMapper mapper;

	public AddRecordingCallable(IChannelMapper aMapper) {
		super(METHOD, TARGET_RESOURCE_PATH, getDefaultParametersInit());
		this.mapper = aMapper;
	}

	private static IRemoteCallParameter[] getDefaultParametersInit() {
		final Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("op", "createEntry");
		params.put(PRIORITY, "Normal");
		params.put(CONFIG_NAME, "mine");
		return RemoteCallParameterFactory.createParameters(params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.kuppe.tvheadend.service.callable.IRCWithResponseHandler#handleResponse
	 * (java.util.Map, byte[])
	 */
	@SuppressWarnings("rawtypes")
	public Object handleResponse(Map responseHeaders, byte[] responseBody) throws NotSerializableException {
		try {
			final JSONObject jsonObject = new JSONObject(new String(responseBody));
			return jsonObject.getInt("success") == 1;
		} catch (JSONException e) {
			throw new NotSerializableException("JSON array parse exception: " + e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ecf.remoteservice.client.IRemoteCallParameterSerializer#serializeParameter(java.lang.String, org.eclipse.ecf.remoteservice.IRemoteCall, org.eclipse.ecf.remoteservice.client.IRemoteCallable, org.eclipse.ecf.remoteservice.client.IRemoteCallParameter[], java.lang.Object[])
	 */
	public IRemoteCallParameter[] serializeParameter(String uri, IRemoteCall call, IRemoteCallable callable,
			IRemoteCallParameter[] currentParam, Object[] paramToSerialize) throws NotSerializableException {
		final List<IRemoteCallParameter> res = new ArrayList<IRemoteCallParameter>();
		res.addAll(Arrays.asList(currentParam));

		for (Object object : paramToSerialize) {
			if (object instanceof Recording) {
				final Recording r = (Recording) object;

				final String channelName = r.getChannel();
				res.add(new RemoteCallParameter(CHANNELID, mapper.getChannelId(channelName)));

				res.add(new RemoteCallParameter(TITLE, r.getTitle()));

				// Calculate start date from starttime (which contains
				// the date too) and format according to backend
				cal.setTime(r.getStart());
				// 1-based month of year
				int month = cal.get(Calendar.MONTH) + 1;
				int day = cal.get(Calendar.DAY_OF_MONTH);
				int year = cal.get(Calendar.YEAR);
				// MM/DD/YYYY
				String date = String.format("%02d/%02d/%04d", month, day, year);
				res.add(new RemoteCallParameter(DATE, date));

				cal.setTime(r.getStart());
				int hour = cal.get(Calendar.HOUR_OF_DAY) + DST_OFFSET;
				int min = cal.get(Calendar.MINUTE);
				// HH:MM
				String start = String.format("%02d:%02d", hour, min);
				res.add(new RemoteCallParameter(STARTTIME, start));

				cal.setTime(r.getEnd());
				hour = cal.get(Calendar.HOUR_OF_DAY) + DST_OFFSET;
				min = cal.get(Calendar.MINUTE);
				// HH:MM
				String end = String.format("%02d:%02d", hour, min);
				res.add(new RemoteCallParameter(STOPTIME, end));
			}
		}
		return res.toArray(new IRemoteCallParameter[res.size()]);
	}
}
