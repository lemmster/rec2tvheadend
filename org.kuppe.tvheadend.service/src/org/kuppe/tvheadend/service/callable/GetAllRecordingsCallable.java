/*
 * Copyright (c) 2013, Markus Alexander Kuppe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.kuppe.tvheadend.service.callable;

import java.io.NotSerializableException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.ecf.remoteservice.client.IRemoteCallParameter;
import org.eclipse.ecf.remoteservice.client.RemoteCallParameterFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kuppe.tv.recording.Recording;
import org.kuppe.tvheadend.service.model.TvHeadEndRecording;

public class GetAllRecordingsCallable extends RecordingCallable implements IRCWithResponseHandler {

	private static final String END_TIME = "end";
	private static final String START_TIME = "start";
	private static final String CHANNEL_NAME = "channel";
	private static final String RECORDING_ID = "id";

	private static final String METHOD = "getRecordings";
	private static final String TARGET_RESOURCE_PATH = "/dvrlist_upcoming";

	public GetAllRecordingsCallable() {
		super(METHOD, TARGET_RESOURCE_PATH, getDefaultParametersInit());
	}

	private static IRemoteCallParameter[] getDefaultParametersInit() {
		// TODO suppport more than 100 recordings (if ever needed)
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("start", "0");
		params.put("limit", "100");
		return RemoteCallParameterFactory.createParameters(params);
	}

	@SuppressWarnings("rawtypes")
	public Object handleResponse(Map responseHeaders, byte[] responseBody) throws NotSerializableException {
		try {
			final JSONObject jsonObject = new JSONObject(new String(responseBody));
			final JSONArray jsonArray = jsonObject.getJSONArray("entries");
			final int count = jsonObject.getInt("totalCount");

			final List<Recording> recordings = new ArrayList<Recording>(count);

			for (int idx = 0; idx < count; idx++) {
				final JSONObject r = (JSONObject) jsonArray.get(idx);
				recordings.add(json2Recording(r));
			}

			return recordings;
		} catch (JSONException e) {
			throw new NotSerializableException("JSON array parse exception: " + e.getMessage());
		}
	}

	private Recording json2Recording(JSONObject json) throws JSONException {
		final int recordingId = json.getInt(RECORDING_ID);
		final String title = json.getString(TITLE);
		final String config = json.getString(CONFIG_NAME);
		final String priority = json.getString(PRIORITY);

		// tvheadend appears to append language information to the channel name
		// when the epg name has a different name for the channel
		final String channel = json.getString(CHANNEL_NAME);

		// tvheadend backend sends timestamps based on
		// seconds since 1970-01-01 00:00:00 UTC (epoch)
		final long startTS = json.getLong(START_TIME);
		final long endTS = json.getLong(END_TIME);
		// Java's date uses milliseconds, hence multiply
		final Date start = new Date(startTS * 1000L);
		final Date end = new Date(endTS * 1000L);

		return new TvHeadEndRecording(recordingId, channel, start, end, title, priority, config);
	}
}
