/*
 * Copyright (c) 2013, Markus Alexander Kuppe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.kuppe.tvheadend.service;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.ecf.core.IContainer;
import org.eclipse.ecf.core.IContainerFactory;
import org.eclipse.ecf.core.security.ConnectContextFactory;
import org.eclipse.ecf.core.util.ECFException;
import org.eclipse.ecf.remoteservice.IRemoteService;
import org.eclipse.ecf.remoteservice.IRemoteServiceRegistration;
import org.eclipse.ecf.remoteservice.client.IRemoteCallable;
import org.eclipse.ecf.remoteservice.client.IRemoteServiceClientContainerAdapter;
import org.eclipse.ecf.remoteservice.rest.client.RestClientContainer;
import org.kuppe.tv.recording.Recording;
import org.kuppe.tvheadend.ITVHeadEnd;
import org.kuppe.tvheadend.channel.IChannelMapper;
import org.kuppe.tvheadend.service.callable.TVHeadEndCallables;

public class TVheadEndService implements ITVHeadEnd {
	
	private static final String TARGET_ADDRESS = System.getProperty("TVheadEndService.TARGET_ADDRESS",
			"http://localhost:9981");
	private static final String REST_CONTAINER_TYPE = "ecf.rest.client";

	private static final String password = "osgiclient";
	private static final String username = "osgiclient";

	private ITVHeadEnd tvHeadEnd = new DummyTVHeadEnd();
	private IChannelMapper mapper;

	/* (non-Javadoc)
	 * @see org.kuppe.tvheadend.ITVHeadEnd#addRecording(org.kuppe.tvheadend.model.Recording)
	 */
	public boolean addRecording(Recording aRecording) {
		return tvHeadEnd.addRecording(aRecording);
	}

	/* (non-Javadoc)
	 * @see org.kuppe.tvheadend.ITVHeadEnd#getRecordings()
	 */
	public List<Recording> getRecordings() {
		return tvHeadEnd.getRecordings();
	}

	/* (non-Javadoc)
	 * @see org.kuppe.tvheadend.ITVHeadEnd#deleteRecording(org.kuppe.tvheadend.model.Recording)
	 */
	public boolean deleteRecording(Recording aRecording) {
		return tvHeadEnd.deleteRecording(aRecording);
	}
	
	public void setChannelMapper(IChannelMapper aMapper) {
		this.mapper = aMapper;
	}
	
	// Called by DS
	public void setContainerFactory(final IContainerFactory factory) throws ECFException {
		Assert.isNotNull(factory);

		final IContainer container = factory.createContainer(REST_CONTAINER_TYPE, TARGET_ADDRESS);
		final IRemoteServiceClientContainerAdapter adapter = (IRemoteServiceClientContainerAdapter) container
				.getAdapter(IRemoteServiceClientContainerAdapter.class);
		
		// TVHeadEnd might require authentication
		adapter.setConnectContextForAuthentication(ConnectContextFactory.createUsernamePasswordConnectContext(username,
				password));

		TVHeadEndCallables tvHeadEndCallables = new TVHeadEndCallables();
		
		// Use specialized parameter to convert a Recording instance into key value pairs
		adapter.setParameterSerializer(tvHeadEndCallables);
		
		// Setup resource handler
		adapter.setResponseDeserializer(tvHeadEndCallables);
		
		// We always want to send default parameters
		RestClientContainer rcc = (RestClientContainer) container;
		rcc.setAlwaysSendDefaultParameters(true);

		IRemoteCallable[] callables = tvHeadEndCallables.getCallables(adapter, mapper);

		final IRemoteServiceRegistration registration = adapter.registerCallables(
				new String[] { ITVHeadEnd.class.getName() }, new IRemoteCallable[][] { callables }, null);
		
		final IRemoteService restClientService = adapter.getRemoteService(registration.getReference());
		tvHeadEnd = (ITVHeadEnd) restClientService.getProxy();
	}
}
