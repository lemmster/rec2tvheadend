/*
 * Copyright (c) 2013, Markus Alexander Kuppe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.kuppe.scheduler.internal;

import java.util.ArrayList;
import java.util.List;

import org.kuppe.scheduler.ISchedulee;
import org.osgi.service.log.LogService;

public class SchedulerImpl {

	private final List<ISchedulee> s = new ArrayList<ISchedulee>();
	private LogService logService;

	public void execute() {
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						synchronized (SchedulerImpl.this) {
							for (ISchedulee schedulee : s) {
								try {
									schedulee.run();
								} catch (Exception e) {
									logService.log(LogService.LOG_WARNING, "Failed to run schedulee", e);
									// Don't fail all if one behaves incorrectly
									e.printStackTrace();
								}
							}
						}
						Thread.sleep(30 * 60 * 1000L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}, "org.kuppe.scheduler").start();
	}

	public void setLogService(LogService aLogService) {
		this.logService = aLogService;
	}

	public synchronized void addSchedulee(ISchedulee schedulee) {
		s.add(schedulee);
	}

	public synchronized void removeSchedulee(ISchedulee schedulee) {
		s.remove(schedulee);
	}
}
