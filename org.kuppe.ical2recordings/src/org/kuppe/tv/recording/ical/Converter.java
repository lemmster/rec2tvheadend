/*
 * Copyright (c) 2013, Markus Alexander Kuppe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.kuppe.tv.recording.ical;

import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.property.DateProperty;

import org.kuppe.tv.recording.Recording;

public class Converter {

	private static String getChannel(String summary) {
		int idx = summary.lastIndexOf("on ");
		return summary.substring(idx + 3);
	}

	private static String getTitle(String summary) {
		int idx = summary.lastIndexOf(" on");
		String substring = summary.substring(0, idx); // "on" + whitespace
		return replaceUmlaut(substring);
	}

	public static Recording getRecording(Component component) {

		// Start date
		final DateProperty dtStart = (DateProperty) component.getProperty(Property.DTSTART);

		// End date
		final DateProperty dtEnd = (DateProperty) component.getProperty(Property.DTEND);

		// Channel and title (both in summary)
		final String summary = component.getProperty(Property.SUMMARY).getValue();
		final String channel = getChannel(summary);
		final String title = getTitle(summary);

		return new Recording(channel, dtStart.getDate(), dtEnd.getDate(), title);
	}

	/**
	 * Replaces all german umlaute in the input string with the usual
	 * replacement scheme, also taking into account capitilization. A test
	 * String such as "Käse Köln Füße Öl Übel Äü Üß ÄÖÜ Ä Ö Ü ÜBUNG" will yield
	 * the result
	 * "Kaese Koeln Fuesse Oel Uebel Aeue Uess AEOEUe Ae Oe Ue UEBUNG"
	 * 
	 * @param input
	 * @return the input string with replaces umlaute
	 * 
	 * @see http://gordon.koefner.at/blog/coding/replacing-german-umlauts/
	 */
	private static String replaceUmlaut(String input) {

		// replace all lower Umlauts
		String o_strResult = input.replaceAll("ü", "ue").replaceAll("ö", "oe").replaceAll("ä", "ae")
				.replaceAll("ß", "ss");

		// first replace all capital umlaute in a non-capitalized context (e.g.
		// Übung)
		o_strResult = o_strResult.replaceAll("Ü(?=[a-zäöüß ])", "Ue").replaceAll("Ö(?=[a-zäöüß ])", "Oe")
				.replaceAll("Ä(?=[a-zäöüß ])", "Ae");

		// now replace all the other capital umlaute
		o_strResult = o_strResult.replaceAll("Ü", "UE").replaceAll("Ö", "OE").replaceAll("Ä", "AE");

		return o_strResult;
	}
}
