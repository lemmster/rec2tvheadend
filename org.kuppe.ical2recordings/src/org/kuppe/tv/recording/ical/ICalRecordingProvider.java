/*
 * Copyright (c) 2013, Markus Alexander Kuppe
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.kuppe.tv.recording.ical;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.ComponentList;

import org.kuppe.tv.recording.Recording;
import org.kuppe.tv.recording.provider.IRecordingProvider;

public class ICalRecordingProvider implements IRecordingProvider {

	private static final String calendarLocation = "http://en.timefor.tv/ical/14bbcd0e8b41e0026626409c57543384.ics";

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kuppe.tv.recording.provider.RecordingProvider#getRecordings()
	 */
	@SuppressWarnings("unchecked")
	public List<Recording> getRecordings() {
		URL url;
		try {
			url = new URL(calendarLocation);
		} catch (MalformedURLException e) {
			// Not expected to happen
			e.printStackTrace();
			return new ArrayList<Recording>();
		}

		final Calendar calendar = getCalendar(url);
		
		final List<Recording> res = new ArrayList<Recording>();

		final ComponentList components = calendar.getComponents();
		for (Iterator<Component> i = components.iterator(); i.hasNext();) {
			final Recording r = Converter.getRecording(i.next());
			res.add(r);
		}

		return res;
	}

	private Calendar getCalendar(URL location) {
		Calendar calendar = new Calendar();
		try {
			final URLConnection connection = location.openConnection();
			final InputStream fin = connection.getInputStream();
			final CalendarBuilder builder = new CalendarBuilder();
			calendar = builder.build(fin);
			fin.close();
		} catch (MalformedURLException doesNotHappen) {
			doesNotHappen.printStackTrace();
		} catch (IOException e) {
			// An IOException happens, when the calendar cannot be download by
			// the HttpURLConnection (e.g. caused by a server outage). 
			e.printStackTrace();
			throw new RuntimeException("Calendar not available", e);
		} catch (ParserException e) {
			// If the remote host serves garbage
			e.printStackTrace();
			throw new RuntimeException("Failed to parse Calendar", e);
		}
		return calendar;
	}
}
